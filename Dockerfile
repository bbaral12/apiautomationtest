FROM node:latest

# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
COPY . .

# install dependencies
RUN npm install
RUN npm install cucumber
RUN npm install supertest

# tell the port number the container should expose
EXPOSE 8080

CMD npm run testparallel
