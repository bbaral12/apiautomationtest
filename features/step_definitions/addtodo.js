BASE_URL = 'http://jsonplaceholder.typicode.com'
const request = require('supertest')(BASE_URL);
const assert = require('assert');
const {Given, When, Then} = require('cucumber');
var relayObj, relayObj_1;

//Run a GET requst on the API for users
Given ('The API and users table', {timeout: 60 * 1000}, async function() {
    let usrresp = await request
        .get('/users')
        .expect(200);

    relayObj = usrresp.body;
});

//Find the id for the user given
When ('I have a {string}', {timeout: 60 * 1000}, async function(username1) {
    var i=0;
    var successflag = false;
    for(i; i<relayObj.length; i++){
        if (relayObj[i].username === username1){
            successflag = true;
            break;
        }
    };
    //Checks userid is found in the body of the get request
    assert.equal(successflag, true, 'Did not find username.');

    relayObj = relayObj[i].id
});

//Find details for specifi userid
When ('I run a get request', async function () {
    let usrresp = await request
        .get('/users/'+relayObj)
        .expect(200);
    
    relayObj = usrresp.body;
});

//Check Fullname and email address matches
Then ('I get the {string} and {string}', {timeout: 60 * 1000}, async function(fullname, emailadd) {
    assert.strictEqual(relayObj.name, fullname, 'Full Name does not match.');
    assert.strictEqual(relayObj.email, emailadd, 'Email address does not match');
});

//Add the todo title and status to the user
When ('I add a todo with a {string} with status {string}', {timeout: 60 * 1000}, async function(todoTitle, todoStatus) {
    let usrresp = await request
        .post('/todos')
        .set('Content-type', 'application/json')
        .send({
            'userId': relayObj,
            'title': todoTitle,
            'completed': todoStatus
        })
        .expect(201);

    relayObj_1 = usrresp.body;//Keep relayObj as we need userid for assertions later
});

//Check the todo title is added to the user with the given status and also as an endpoint to the todo table
Then ('The {string} is added to the given user with the given {string}', {timeout: 60 * 1000}, async function(todoTitle, todoStatus) {
    assert.strictEqual(relayObj_1.id, 201, 'No todo was added');//200 todos before, todo id for new todo should auto populate as 201
    assert.strictEqual(relayObj_1.userId, relayObj, 'User id does not match');
    assert.strictEqual(relayObj_1.title, todoTitle, 'Todo title does not match.');
    assert.strictEqual(relayObj_1.completed, todoStatus, 'Todo status does not match');
});

//Run a GET requst on the API for todos
Given ('The API and todos table', {timeout: 60 * 1000}, async function() {
    let usrresp = await request
        .get('/todos')
        .expect(200);

    relayObj = usrresp.body;
});

//Find the id for the given todo
When ('I have a todo title {string}', {timeout: 60 * 1000}, async function(todotitle) {
    var i=0;
    var successflag = false;
    for(i; i<relayObj.length; i++){
        if (relayObj[i].title === todotitle){
            successflag = true;
            break;
        }
    };
    //Checks todo title is found in the body of the get request
    assert.equal(successflag, true, 'Did not find todo');

    relayObj = relayObj[i].id
});

//Change the status of the todo
When ('I send a request to change the status to {string}', {timeout: 60 * 1000}, async function(todoStatus) {
    let usrresp = await request
        .put('/todos/' + relayObj)
        .set('Content-type', 'application/json')
        .send({
            'completed': todoStatus
        })
        .expect(200);

    relayObj_1 = usrresp.body//Keep relayObj as we need todo id for assertions later
});

//Check the status of the todo is changed to given status
Then ('The status of the given todo is changed to given status {string}', {timeout: 60 * 1000}, async function(todoStatus) {
    assert.strictEqual(relayObj_1.id, relayObj, 'Todo id does not match');
    assert.strictEqual(relayObj_1.completed, todoStatus, 'Todo status does not match');
});

//Send a Delete request to delete the todo
When ('I send a request to delete the todo', {timeout: 60 * 1000}, async function() {
    let usrresp = await request
        .delete('/todos/' + relayObj)
        .expect(200);

    relayObj=usrresp.body;
});

//Check the id of todo to confirm it is deleted
Then ('The todo is deleted', {timeout: 60 * 1000}, async function() {
    assert.equal(relayObj.id, null, 'Todo was not deleted');
});