Feature: Manage the todo features

    Scenario Outline: Get details of a user when given username
        Given The API and users table
        When I have a '<username>'
        When I run a get request
        Then I get the '<fullname>' and '<emailadd>'
        
        Examples:
        |username   |fullname           |emailadd                   |
        |Bret       |Leanne Graham      |Sincere@april.biz          |
        |Karianne   |Patricia Lebsack   |Julianne.OConner@kory.org  |
        |Delphine   |Glenna Reichert    |Chaim_McDermott@dana.io    |

    Scenario Outline: Add a todo to a user
        Given The API and users table
        When I have a '<username>'
        When I add a todo with a '<todoTitle>' with status '<todoStatus>'
        Then The '<todoTitle>' is added to the given user with the given '<todoStatus>'

        Examples:
        |username   |todoTitle            |todoStatus |
        |Bret       |Test todo title 1    |false      |
        |Karianne   |Test todo title 2    |true       |
        |Delphine   |Test todo title 3    |true       |

    Scenario Outline: Change the status of a todo
        Given The API and todos table
        When I have a todo title '<todoTitle>'
        When I send a request to change the status to '<todoStatus>'
        Then The status of the given todo is changed to given status '<todoStatus>'

        Examples:
        |todoTitle                                          |todoStatus |
        |voluptas quo tenetur perspiciatis explicabo natus  |false      |
        |distinctio exercitationem ab doloribus             |true       |
        |voluptas consequatur qui ut quia magnam nemo esse  |false      |

    Scenario: Delete a todo
        Given The API and todos table
        When I have a todo title 'sit reprehenderit omnis quia'
        When I send a request to delete the todo
        Then The todo is deleted