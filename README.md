# APIAutomationTest

Introduction

	The test runs basic test cases against the api https://jsonplaceholder.typicode.com/ using a cucumber framework.

How to run the tests:

	API Automation  Test

		-Run with artifacts from gitlab
			--Clone the project

				1) Run as a single thread
					-Run the following command from the project directory.
						npm run test
				
				2) Run in parallel
					-Run the following command from the the project directory.
						npm run testparallel(parallel threads hardcoded to 4)

		-Run with a docker image
			--Pull the docker image bbaral12/apiauto
			--Run the following command
				docker run bbaral12/apiauto